﻿using System;
namespace MyArray
{
    public class ArrayStack<T>
    {
        BaseArray<T> _array;
        int _used = 0;

        public ArrayStack(int length)
        {
            _array = new BaseArray<T>(length);
            _used = 0;
        }

        
        public T this[int i]
        {
            set { _array[i] = value; }
            get { return _array[i]; }
        }

        public void Add(int i, T x)
        {
            if ( _used + 1 > _array.Length())
            {
                Resize();
            }

            for(int j = _used; j>i; j--)
            {
                _array[j] = _array[j - 1];
            }
            _array[i] = x;

            _used++ ;
        }

        public bool IsEmpty() => (_used == 0);

        /// <summary>
        /// enlarge based array T _array[]
        /// </summary>
        public void Resize()
        {
            int len = _array.Length();
            if (len < 1)
            {
                _array.Resize(1);
            }
            else
            {
                _array.Resize(len * 2);
            }
        }

        public T Remove(int i)
        {
            T x = _array[i];
            for(int j = i; j < _used - 1; j++)
            {
                _array[j] = _array[j + 1];

            }
            _used--;
            return x;
        }
    }
}
