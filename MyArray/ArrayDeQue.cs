﻿using System;
namespace MyArray
{
    public class ArrayDeQue<T>
    {
        BaseArray<T> _array;
        int _start = 0;
        int _used = 0;

        public ArrayDeQue(int length)
        {
            _array = new BaseArray<T>(length);
            _start = 0;
            _used = 0;
        }

        public T this[int i]
        {
            set { _array[(i+_start)%_array.Length()] = value; }
            get { return _array[(i + _start) % _array.Length()]; }
        }

        public void Add(int i, T x)
        {
            if (_used + 1 >= _array.Length()) Resize();
            if (i < _used / 2)
            { // a[0],..,a[i-1] を左に 1 つずらす
                _start = (_start == 0) ? _array.Length() - 1 : _start - 1;
                for (int k = 0; k <= i - 1; k++)
                    _array[(_start + k) % _array.Length()] = _array[(_start + k + 1) % _array.Length()];
            }
            else
            { // a[i],..,a[n-1] を右に 1 つずらす
                for (int k = _used; k > i; k--)
                    _array[(_start + k) % _array.Length()] = _array[(_start + k - 1) % _array.Length()];
            }
            _array[(_start + i) % _array.Length()] = x;
            _used++;
        }

        public void Resize()
        {
            int len = _array.Length();
            if (len < 1)
            {
                _array.Resize(1);
            }
            else
            {
                BaseArray<T> work = _array.Copy();
                int work_used = _used;
                int work_start = _start;
                int work_length = _array.Length();

                _array.Resize(len * 2);
                _start = 0;

                for (int i = 0; i < work_used; i++)
                {
                    _array[Index(i)] = work[IndexForWork(i + work_start, work_length)];
                }

            }
        }

        private int IndexForWork(int i, int length) => ((i) % length);
        private int Index(int i) => i % _array.Length();
    }
}
