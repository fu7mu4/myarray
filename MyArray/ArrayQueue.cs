﻿using System;
namespace MyArray
{
    public class ArrayQueue<T>
    {
        BaseArray<T> _array;
        int _start = 0;
        int _used = 0;

        public ArrayQueue(int length)
        {
            _array = new BaseArray<T>(length);
            _start = 0;
            _used = 0;
        }


        public T this[int i]
        {
            set { _array[i] = value; }
            get { return _array[i]; }
        }

        private int IndexWhenAdd() => ((_used + _start) % _array.Length());

        private int IndexForWork(int i, int length) => ((i) % length);
        private int Index(int i) => i % _array.Length();

        public bool Add(T x)
        {
            if(_used + 1 >= _array.Length() )
            {
                Resize();
            }

            _array[IndexWhenAdd()] = x;
            _used++;
            return true;
        }

        public T Remove()
        {
            T x = _array[_start];
            _start = Index(_start + 1);
            _used--;

            return x;
        }

        public bool IsEmpty() => _used == 0;

        public void Resize()
        {
            int len = _array.Length();
            if (len < 1)
            {
                _array.Resize(1);
            }
            else
            {
                BaseArray<T> work = _array.Copy();
                int work_used = _used;
                int work_start = _start;
                int work_length = _array.Length();

                _array.Resize(len * 2);
                _start = 0;
                
                for(int i=0; i< work_used; i++)
                {
                    _array[Index(i)] = work[IndexForWork(i + work_start, work_length)];
                }

            }
        }
    }
}
