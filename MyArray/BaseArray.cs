﻿using System;
namespace MyArray
{
    public class BaseArray<T>
    {
        private T[] a;

         /// <summary>
         ///  Allocate memory with len Ts
         /// </summary>
         /// <param name="len">number of T</param>
        public BaseArray(int len)
        {
            a = new T[len];
        }

         public bool IsNullOrEmpty()
        {
            return  (a == null ) || ( Length() == 0);
        }

        public int  Length()
        {
            return a.Length;
        }

         /// <summary>
         /// indexer of MyArray
         /// </summary>
         /// <param name="i">an index</param>
         /// <returns></returns>
        public T this[int i]
        {
            set { a[i] = value; }
            get { return a[i]; }
        }

         public void MoveTo(BaseArray<T> b)
        {
            b.a = a;
            a = null;
        }

         public void MoveFrom(BaseArray<T> b)
        {
            a = b.a;
            b.a = null;
        }

        public void Resize(int newSize)
        {
            Array.Resize<T>(ref a, newSize);
        }

        public BaseArray<T> Copy()
        {
            BaseArray<T> res = new BaseArray<T>(Length());
            Array.Copy(a, res.a, Length());
            return res;


        }
    }
}
