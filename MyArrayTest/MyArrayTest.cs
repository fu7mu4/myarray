using NUnit.Framework;

namespace MyArrayTest
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ZeroLengthBaseArray()
        {
            //Assert.Pass();
            MyArray.BaseArray<string> ex = new MyArray.BaseArray<string>(0);
            Assert.AreEqual(ex.Length(), 0);
        }
    }
}